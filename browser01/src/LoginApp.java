import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginApp extends Application {

    // private static final Class<? extends Application> App = null;
    // MySQL database connection
    private Connection connection;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        /* */ primaryStage.setTitle("Login Application");

        // Database connection
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/userCredential", "root",
                    "123456");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // Create a sign-up form
        VBox signUpForm = createSignUpForm();

        // Create a sign-in form
        VBox signInForm = createSignInForm();

        // Create a tab pane for sign-up and sign-in
        TabPane tabPane = new TabPane();
        Tab signUpTab = new Tab("Sign Up", signUpForm);
        Tab signInTab = new Tab("Sign In", signInForm);
        tabPane.getTabs().addAll(signUpTab, signInTab);

        // Set up the scene
        StackPane root = new StackPane(tabPane);
        Scene scene = new Scene(root, 300, 200);
        primaryStage.setScene(scene);

        primaryStage.show();
    }

    private VBox createSignUpForm() {
        VBox signUpForm = new VBox(10);
        signUpForm.setPadding(new Insets(10));
        TextField usernameField = new TextField();
        PasswordField passwordField = new PasswordField();
        Button signUpButton = new Button("Sign Up");

        signUpButton.setOnAction(event -> {
            String username = usernameField.getText();
            String password = passwordField.getText();

            try {
                PreparedStatement preparedStatement = connection
                        .prepareStatement("INSERT INTO users (username, password) VALUES (?, ?)");
                preparedStatement.setString(1, username);
                preparedStatement.setString(2, password);
                preparedStatement.executeUpdate();
                preparedStatement.close();
                System.out.println("User registered.");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        signUpForm.getChildren().addAll(new Label("Username:"), usernameField, new Label("Password:"), passwordField,
                signUpButton);

        return signUpForm;
    }

    private VBox createSignInForm() {
        VBox signInForm = new VBox(10);
        signInForm.setPadding(new Insets(10));
        TextField usernameField = new TextField();
        PasswordField passwordField = new PasswordField();
        Button signInButton = new Button("Sign In");

        signInButton.setOnAction(event -> {
            String username = usernameField.getText();
            String password = passwordField.getText();

            try {
                PreparedStatement preparedStatement = connection
                        .prepareStatement("SELECT * FROM users WHERE username = ? AND password = ?");
                preparedStatement.setString(1, username);
                preparedStatement.setString(2, password);
                ResultSet resultSet = preparedStatement.executeQuery();

                if (resultSet.next()) {
                    System.out.println("Login successful.");
                } else {
                    System.out.println("Login failed.");
                }

                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        signInForm.getChildren().addAll(new Label("Username:"), usernameField, new Label("Password:"), passwordField,
                signInButton);

        return signInForm;
    }

    @Override
    public void stop() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
