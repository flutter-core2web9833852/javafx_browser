import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class browser00 extends Application {
    private WebView webView;
    private WebEngine webEngine;
    private TextField urlField;
    private Button goButton;
    private ListView<String> bookmarks;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        primaryStage.setTitle("Better Browser");

        webView = new WebView();
        webEngine = webView.getEngine();

        urlField = new TextField();
        urlField.setPromptText("Enter URL");
        urlField.setPrefWidth(400);
        goButton = new Button("Go");
        goButton.setMinWidth(60);
        goButton.setOnAction(event -> loadURL());

        bookmarks = new ListView<>();
        bookmarks.setMinWidth(180);

        VBox topBox = new VBox(10);
        topBox.getChildren().addAll(createHeader(), createNavigationPane());

        BorderPane borderPane = new BorderPane();
        borderPane.setTop(topBox);
        borderPane.setCenter(webView);
        borderPane.setRight(bookmarks);

        Scene scene = new Scene(borderPane, 800, 600);
        primaryStage.setScene(scene);

        loadInitialPage();
        setupBookmarks();

        primaryStage.show();
    }

    private HBox createHeader() throws FileNotFoundException {
        HBox header = new HBox(10);
        header.setPadding(new Insets(10, 10, 0, 10));

        Label titleLabel = new Label("Better Browser");
        titleLabel.setFont(Font.font("Arial", 18));
        titleLabel.setTextFill(Color.DARKBLUE);

        InputStream stream = new FileInputStream("D:\\demoProject\\browser01\\browser01\\src\\icon.png");
        Image image = new Image(stream);

        ImageView logo = new ImageView(image);
        logo.setFitHeight(30);
        logo.setFitWidth(30);

        header.getChildren().addAll(logo, titleLabel);
        return header;
    }

    private HBox createNavigationPane() {
        HBox navigationPane = new HBox(10);
        navigationPane.setPadding(new Insets(0, 10, 10, 10));

        urlField.setStyle("-fx-font-size: 14;");
        goButton.setStyle("-fx-font-size: 14;");

        HBox.setHgrow(urlField, Priority.ALWAYS);
        navigationPane.getChildren().addAll(urlField, goButton);
        return navigationPane;
    }

    private void loadInitialPage() {
        webEngine.load("https://www.example.com");
    }

    private void loadURL() {
        String url = urlField.getText();
        if (!url.isEmpty()) {
            webEngine.load(url);
            addBookmark(url);
        }
    }

    private void setupBookmarks() {
        bookmarks.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                webEngine.load(newValue);
            }
        });
    }

    private void addBookmark(String url) {
        if (!bookmarks.getItems().contains(url)) {
            bookmarks.getItems().add(url);
        }
    }
}
