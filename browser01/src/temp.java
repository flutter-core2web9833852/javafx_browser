
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.PreparedStatement;
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
import java.sql.SQLException;
//import java.sql.Statement;
import java.util.Optional;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
//import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
//import javafx.scene.layout.BorderPane;
import javafx.event.EventHandler;
import javafx.geometry.Insets;

public class temp extends Application {

    private Connection connection;
    private ListView<String> bookmarks;
    private WebEngine webEngine;
    private String url;
    private Label label;
    // private TextField urlField;
    private Button goButton;
    private String str;
    private TabPane tabPane;
    // private int count;
    private VBox newTab;
    private int flag;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws FileNotFoundException {

        primaryStage.setTitle("Baap Browser");
        /*
         * establishDatabaseConnection();
         * 
         * // primaryStage.setTitle("My Browser");
         * 
         * WebView webView = new WebView();
         * webEngine = webView.getEngine();
         * 
         * // urlField = new TextField();
         * // urlField.setPromptText("Enter URL");
         * // urlField.setPrefWidth(400);
         * goButton = new Button("Go");
         * goButton.setMinWidth(60);
         * 
         * TextField addressBar = new TextField();
         * addressBar.setOnAction(event -> {
         * url = addressBar.getText();
         * addressBar.setPromptText("Enter text");
         * addressBar.setPrefWidth(400);
         * str = url;
         * if (!url.startsWith("http://") && !url.startsWith("https://")) {
         * url = "https://" + url + ".com";
         * }
         * 
         * webEngine.load(url);
         * addBookmark(url);
         * 
         * try {
         * // Class.forName("org.h2.Driver");
         * Class.forName("com.mysql.cj.jdbc.Driver");
         * if (connection != null) {
         * String query = "INSERT INTO history (query) VALUES (?)";
         * PreparedStatement preparedStatement = connection.prepareStatement(query);
         * preparedStatement.setString(1, str);
         * preparedStatement.executeUpdate();
         * preparedStatement.close();
         * }
         * } catch (Exception e) {
         * e.printStackTrace();
         * }
         * 
         * });
         * 
         * // goButton.setOnAction(event -> webEngine.load(url));
         * 
         * Button backButton = new Button("Back");
         * backButton.setOnAction(event -> webEngine.executeScript("history.back()"));
         * 
         * Button forwardButton = new Button("Forward");
         * forwardButton.setOnAction(event ->
         * webEngine.executeScript("history.forward()"));
         * 
         * Button refreshButton = new Button("Refresh");
         * refreshButton.setOnAction(event -> webEngine.reload());
         * 
         * // Button Search = new Button("Search");
         * // Search.setOnAction(event -> loadURL());
         * 
         * this.label = new Label();
         * 
         * Button button = new Button("Incognito");
         * 
         * button.setOnAction(new EventHandler<ActionEvent>() {
         * 
         * @Override
         * public void handle(ActionEvent event) {
         * showConfirmation();
         * }
         * });
         */
        Button tabbutton = new Button("new tab");

        tabbutton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                flag = 1;
                tabPane = new TabPane();
                Tab obj = new Tab("123", newTab);
                tabPane.getTabs().addAll(obj);
            }
        });

        if (flag == 1) {
            newTab = mtab();
        }

        VBox topBox = new VBox();
        topBox.getChildren().add(tabbutton);

        StackPane root = new StackPane(tabPane);
        Scene scene = new Scene(root, 300, 200);
        primaryStage.setScene(scene);

        primaryStage.show();

        /*
         * // Button radioButton = new Button("Enable history");
         * // radioButton.setOnAction(event ->
         * webEngine.executeScript("history.disable"));
         * 
         * bookmarks = new ListView<>();
         * 
         * VBox topBox = new VBox(10);
         * topBox.getChildren().addAll(createHeader(), createNavigationPane());
         * 
         * HBox navigationBar = new HBox(backButton, forwardButton, refreshButton,
         * button, goButton);
         * VBox layout = new VBox(navigationBar, addressBar, webView, createHeader());
         * 
         * BorderPane borderPane = new BorderPane();
         * borderPane.setTop(topBox);
         * borderPane.setTop(layout);
         * borderPane.setCenter(webView);
         * borderPane.setRight(bookmarks);
         * 
         * Scene scene = new Scene(borderPane);
         * primaryStage.setScene(scene);
         * 
         * // loadInitialPage();
         * setupBookmarks();
         * 
         * primaryStage.show();
         * 
         * // primaryStage.setOnCloseRequest(event -> closeDatabaseConnection());
         */
        // Scene scene = new Scene(borderPane);
        primaryStage.setScene(scene);

        // loadInitialPage();
        setupBookmarks();

        primaryStage.show();
    }

    private VBox mtab() throws FileNotFoundException {

        // primaryStage.setTitle("Baap Browser");

        establishDatabaseConnection();

        // primaryStage.setTitle("My Browser");

        WebView webView = new WebView();
        webEngine = webView.getEngine();

        // urlField = new TextField();
        // urlField.setPromptText("Enter URL");
        // urlField.setPrefWidth(400);
        goButton = new Button("Go");
        goButton.setMinWidth(60);

        TextField addressBar = new TextField();
        addressBar.setOnAction(event -> {
            url = addressBar.getText();
            addressBar.setPromptText("Enter text");
            addressBar.setPrefWidth(400);
            str = url;
            if (!url.startsWith("http://") && !url.startsWith("https://")) {
                url = "https://" + url + ".com";
            }

            webEngine.load(url);
            addBookmark(url);

            try {
                // Class.forName("org.h2.Driver");
                Class.forName("com.mysql.cj.jdbc.Driver");
                if (connection != null) {
                    String query = "INSERT INTO history (query) VALUES (?)";
                    PreparedStatement preparedStatement = connection.prepareStatement(query);
                    preparedStatement.setString(1, str);
                    preparedStatement.executeUpdate();
                    preparedStatement.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        });

        // goButton.setOnAction(event -> webEngine.load(url));

        Button backButton = new Button("Back");
        backButton.setOnAction(event -> webEngine.executeScript("history.back()"));

        Button forwardButton = new Button("Forward");
        forwardButton.setOnAction(event -> webEngine.executeScript("history.forward()"));

        Button refreshButton = new Button("Refresh");
        refreshButton.setOnAction(event -> webEngine.reload());

        // Button Search = new Button("Search");
        // Search.setOnAction(event -> loadURL());

        this.label = new Label();

        Button button = new Button("Incognito");

        button.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                showConfirmation();
            }
        });

        Button tabbutton = new Button("new tab");

        VBox newTab = mtab();

        button.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                tabPane = new TabPane();
                Tab obj = new Tab("");
                tabPane.getTabs().addAll(obj);
            }
        });

        if (flag == 1) {
            newTab = mtab();
        }

        // VBox topBox = new VBox();
        // topBox.getChildren().add(tabbutton);

        // Button radioButton = new Button("Enable history");
        // radioButton.setOnAction(event -> webEngine.executeScript("history.disable"));

        bookmarks = new ListView<>();

        VBox topBox = new VBox(10);
        topBox.getChildren().addAll(createHeader(), createNavigationPane(), tabbutton);

        HBox navigationBar = new HBox(backButton, forwardButton, refreshButton, button, goButton);
        VBox layout = new VBox(navigationBar, addressBar, webView, createHeader());

        BorderPane borderPane = new BorderPane();
        borderPane.setTop(topBox);
        borderPane.setTop(layout);
        borderPane.setCenter(webView);
        borderPane.setRight(bookmarks);

        // Scene scene = new Scene(borderPane);
        // primaryStage.setScene(scene);

        // loadInitialPage();
        setupBookmarks();
        return newTab;

        // primaryStage.show();
    }

    private HBox createHeader() throws FileNotFoundException {
        HBox header = new HBox(10);
        header.setPadding(new Insets(10, 10, 0, 10));

        Label titleLabel = new Label("bAAp Browser");
        titleLabel.setFont(Font.font("Arial", 18));
        titleLabel.setTextFill(Color.DARKBLUE);

        InputStream stream = new FileInputStream("D:\\demoProject\\browser01\\browser01\\src\\icon.png");
        Image image = new Image(stream);

        ImageView logo = new ImageView(image);
        logo.setFitHeight(30);
        logo.setFitWidth(30);

        header.getChildren().addAll(logo, titleLabel);
        return header;
    }

    private HBox createNavigationPane() {
        HBox navigationPane = new HBox(10);
        navigationPane.setPadding(new Insets(0, 10, 10, 10));

        // url.setStyle("-fx-font-size: 14;");
        goButton.setStyle("-fx-font-size: 14;");

        // HBox.setHgrow(urlField, Priority.ALWAYS);
        // navigationPane.getChildren().addAll(urlField, goButton);
        return navigationPane;
    }

    /*
     * private void loadInitialPage() {
     * webEngine.load(url);
     * addBookmark(url);
     * }
     *
     * private void loadURL() {
     * // String url = urlField.getText();
     * if (!url.isEmpty()) {
     * webEngine.load(url);
     * addBookmark(url);
     * }
     * }
     */
    private void setupBookmarks() {
        bookmarks.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                webEngine.load(newValue);
            }
        });
    }

    private void addBookmark(String url) {
        if (!bookmarks.getItems().contains(url)) {
            bookmarks.getItems().add(str);
        }
    }

    private void establishDatabaseConnection() {
        try {
            String url = "jdbc:mysql://localhost:3306/searchhistory";
            String username = "root";
            String password = "123456";
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void closeDatabaseConnection() {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // Handle closure errors
        }
    }

    private void showConfirmation() {

        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Incognito mode");
        alert.setHeaderText("Search History won't store.");
        // alert.setContentText("C:/MyFile.txt");

        // option != null.
        Optional<ButtonType> option = alert.showAndWait();

        if (option.get() == null) {
            this.label.setText("No selection!");
        } else if (option.get() == ButtonType.OK) {
            // this.label.setText("File deleted!");
            closeDatabaseConnection();
        } else if (option.get() == ButtonType.CANCEL) {
            // this.label.setText("Cancelled!");
            establishDatabaseConnection();
        } else {
            this.label.setText("-");
        }
    }

}
