# JavaFX Browser

## Overview

The JavaFX Browser project is a web browser application developed using JavaFX. It provides a user-friendly interface for browsing the internet with essential features such as forward, backward, reload of web pages, and the ability to maintain a list of open tabs. Additionally, the browser supports search history storage in a MySQL database, an incognito mode for private browsing, and a tab management system.

## Features

- Basic Web Browsing:
  - Navigate through web pages using forward, backward, and reload functions.
  - View and interact with websites within the application.

- Search History:
  - Store and retrieve search history from a MySQL database.
  - Easily access and manage past searches for a more convenient browsing experience.

- Incognito Mode:
  - Browse the internet privately without saving any history or data.
  - Ensure anonymity during private sessions.

- Tab Management:
  - Maintain a list of open tabs for efficient multitasking.
  - Switch between tabs seamlessly.
