
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AlertProg {

    public static void display(String title, String message) {

        Stage window = new Stage();

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setWidth(280);

        Label l = new Label();
        l.setText(message);

        Button bt = new Button("close");
        bt.setOnAction(e -> window.close());

        VBox vb = new VBox(12);
        vb.getChildren().addAll(vb, bt);
        // vb.setAlignment(Pos.CENTER);

        Scene s = new Scene(vb);
        window.setScene(s);
        window.showAndWait();
    }
}
